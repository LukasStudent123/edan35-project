#version 410

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

uniform mat4 vertex_model_to_world;
uniform mat4 normal_model_to_world;
uniform mat4 vertex_world_to_clip;
uniform vec2 sinWobble;

out VS_OUT {
	vec3 vertex;
	vec3 normal;
} vs_out;

void main()
{
	vec3 displaced_vertex = vec3(vertex_model_to_world * vec4(vertex, 1.0));

	displaced_vertex.y += vertex.x * (sinWobble.x / 10.0f);
	displaced_vertex.y += vertex.z * (sinWobble.y / 10.0f);
	vs_out.vertex = displaced_vertex;
	vs_out.normal = normalize(vec3(normal_model_to_world * vec4(normal, 0.0)));

	gl_Position = vertex_world_to_clip * vertex_model_to_world * vec4(vertex, 1.0);
}

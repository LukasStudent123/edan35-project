#version 410

uniform vec3 camera_position;
uniform float fillAmount;

in VS_OUT {
	vec3 vertex;
	vec3 normal;
} fs_in;

out vec4 frag_color;

void main()
{
	vec3 v = normalize(camera_position - fs_in.vertex);
	vec3 n = normalize(fs_in.normal);
	if(dot(v, n) < 0.25f && dot(v, n) > -0.25f) {
		frag_color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		return;
	}
	vec4 color_d = vec4(0.0f, 0.0f, 0.2f, 1.0f);
	vec4 color_s = vec4(0.0f, 0.5f, 0.5f, 1.0f);
	if(fs_in.vertex.y < fillAmount) {
		if(gl_FrontFacing){
			float facing = 1 - max(dot(v, n), 0);
			vec4 water_color = mix(color_d, color_s, facing);
			if (fillAmount - fs_in.vertex.y < 0.04f) {
				frag_color = vec4((water_color.r+0.8f)/2.0f, (water_color.g+0.8f)/2.0f, (water_color.b+0.8f)/2.0f, 1.0f);
			} else {
				frag_color = mix(color_d, color_s, facing);
			}
		} else {
			frag_color = vec4(0.8f, 0.8f, 0.8f, 1.0f);
		}
	} else {
		discard;
	}
}

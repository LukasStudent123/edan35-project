#version 410

uniform samplerCube skybox_texture;

in VS_OUT {
	vec3 texcoords;
} fs_in;

out vec4 frag_color;

void main() {
	frag_color = texture(skybox_texture, fs_in.texcoords);
}

#include "liquid_in_a_container.hpp"
#include "parametric_shapes.hpp"

#include "config.hpp"
#include "core/Bonobo.h"
#include "core/FPSCamera.h"
#include "core/node.hpp"
#include "core/ShaderProgramManager.hpp"

#include <imgui.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <tinyfiledialogs.h>

#include <clocale>
#include <cstdlib>
#include <stdexcept>

float const mv = 0.1f;
float const av = 0.1f;
glm::vec3 const move_velocity = glm::vec3(mv, mv, mv);
glm::vec3 const angular_velocity = glm::vec3(av, av, av);
const float sphere_radius = 1.5f;

edaf80::Assignment3::Assignment3(WindowManager& windowManager) :
	mCamera(0.5f * glm::half_pi<float>(),
	        static_cast<float>(config::resolution_x) / static_cast<float>(config::resolution_y),
	        0.01f, 1000.0f),
	inputHandler(), mWindowManager(windowManager), window(nullptr)
{
	WindowManager::WindowDatum window_datum{ inputHandler, mCamera, config::resolution_x, config::resolution_y, 0, 0, 0, 0};

	window = mWindowManager.CreateGLFWWindow("EDAF80: Assignment 3", window_datum, config::msaa_rate);
	if (window == nullptr) {
		throw std::runtime_error("Failed to get a window: aborting!");
	}

	bonobo::init();
}

edaf80::Assignment3::~Assignment3()
{
	bonobo::deinit();
}

void
edaf80::Assignment3::run()
{
	// Set up the camera
	mCamera.mWorld.SetTranslate(glm::vec3(0.0f, 0.0f, 6.0f));
	mCamera.mMouseSensitivity = glm::vec2(0.003f, 0.003f);
	mCamera.mMovementSpeed = glm::vec3(3.0f, 3.0f, 3.0f); // 3 m/s => 10.8 km/h

	float elapsed_time_s = 0.0f;

	// Create the shader programs
	ShaderProgramManager program_manager;

	GLuint liquid_shader = 0u;
	program_manager.CreateAndRegisterProgram(
		"Liquid",
		{ {ShaderType::vertex, "Project/liquid.vert"},
		  {ShaderType::fragment, "Project/liquid.frag"} },
		liquid_shader
	);
	if (liquid_shader == 0u) {
		LogError("Failed to load liquid shader");
	}

	GLuint fallback_shader = 0u;
	program_manager.CreateAndRegisterProgram("Fallback",
	                                         { { ShaderType::vertex, "common/fallback.vert" },
	                                           { ShaderType::fragment, "common/fallback.frag" } },
	                                         fallback_shader);
	if (fallback_shader == 0u) {
		LogError("Failed to load fallback shader");
		return;
	}

	GLuint skybox_shader = 0u;
	program_manager.CreateAndRegisterProgram(
		"Skybox",
		{ {ShaderType::vertex, "EDAF80/skybox.vert"},
		  {ShaderType::fragment, "EDAF80/skybox.frag"} },
		skybox_shader
	);
	if (skybox_shader == 0u) {
		LogError("Failed to load skybox shader");
	}

	auto light_position = glm::vec3(-2.0f, 4.0f, 2.0f);
	auto const set_uniforms = [&light_position](GLuint program){
		glUniform3fv(glGetUniformLocation(program, "light_position"), 1, glm::value_ptr(light_position));
	};

	auto camera_position = mCamera.mWorld.GetTranslation();
	float fillAmount = 0.0f;
	glm::vec2 sinWobble = glm::vec2(0.0f);
	auto const phong_set_uniforms = [&camera_position,&sinWobble,&fillAmount](GLuint program) {
		glUniform3fv(glGetUniformLocation(program, "camera_position"), 1, glm::value_ptr(camera_position));
		glUniform2fv(glGetUniformLocation(program, "sinWobble"), 1, glm::value_ptr(sinWobble));
		glUniform1f(glGetUniformLocation(program, "fillAmount"), fillAmount);
	};


	auto skybox_shape = parametric_shapes::createSphere(200.0f, 100u, 100u);
	if (skybox_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the skybox");
		return;
	}

	Node skybox;
	skybox.set_geometry(skybox_shape);
	skybox.set_program(&skybox_shader, set_uniforms);
	auto texid = bonobo::loadTextureCubeMap(
		config::resources_path("cubemaps/Teide/posx.jpg"),
		config::resources_path("cubemaps/Teide/negx.jpg"),
		config::resources_path("cubemaps/Teide/posy.jpg"),
		config::resources_path("cubemaps/Teide/negy.jpg"),
		config::resources_path("cubemaps/Teide/posz.jpg"),
		config::resources_path("cubemaps/Teide/negz.jpg")
	);
	skybox.add_texture("skybox_texture", texid, GL_TEXTURE_CUBE_MAP);

	auto demo_shape = parametric_shapes::createSphere(sphere_radius, 400u, 400u);
	if (demo_shape.vao == 0u) {
		LogError("Failed to retrieve the mesh for the demo sphere");
		return;
	}

	Node demo_sphere;
	demo_sphere.set_geometry(demo_shape);
	demo_sphere.set_program(&liquid_shader, set_uniforms);


	glClearDepthf(1.0f);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glEnable(GL_DEPTH_TEST);


	auto lastTime = std::chrono::high_resolution_clock::now();

	std::int32_t demo_sphere_program_index = 0;
	auto cull_mode = bonobo::cull_mode_t::disabled;
	auto polygon_mode = bonobo::polygon_mode_t::fill;
	bool show_logs = true;
	bool show_gui = true;
	bool shader_reload_failed = false;

	changeCullMode(cull_mode);

	auto current_move_velocity = glm::vec3(0.0f, 0.0f, 0.0f);
	auto current_move_velocity2 = glm::vec3(0.0f, 0.0f, 0.0f);
	auto current_angular_velocity = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 current_move_acceleration = glm::vec3(0.0f);
	glm::vec3 current_move_acceleration2 = glm::vec3(0.0f);

	float dtl = 0.0f;
	float dtr = 0.0f;
	float dtu = 0.0f;
	float dtd = 0.0f;
	bool lkey = false;
	bool rkey = false;
	bool ukey = false;
	bool dkey = false;

	bool ldeacc = false;
	bool rdeacc = false;
	bool udeacc = false;
	bool ddeacc = false;
	float deltax = 1.0f;
	float deltax2 = 1.0f;
	float sinx = 0.0f;
	float sinx2 = 0.0f;
	float deltaz = 1.0f;
	float deltaz2 = 1.0f;
	float sinz = 0.0f;
	float sinz2 = 0.0f;

	float varx = 0.0f;
	float varx2 = 0.0f;//Decides the variable x in exp(x)
	float varz = 0.0f;
	float varz2 = 0.0f;//Decides the variable x in exp(x)

	float accel_lerp = 0.0f;

	bool lpress = false;
	bool upress = false;
	bool rpress = false;
	bool dpress = false;

	while (!glfwWindowShouldClose(window)) {
		auto const nowTime = std::chrono::high_resolution_clock::now();
		auto const deltaTimeUs = std::chrono::duration_cast<std::chrono::microseconds>(nowTime - lastTime);
		lastTime = nowTime;
		elapsed_time_s += std::chrono::duration<float>(deltaTimeUs).count();

		float dt = std::chrono::duration<float>(deltaTimeUs).count();

		auto& io = ImGui::GetIO();
		inputHandler.SetUICapture(io.WantCaptureMouse, io.WantCaptureKeyboard);

		glfwPollEvents();
		inputHandler.Advance();
		mCamera.Update(deltaTimeUs, inputHandler);
		camera_position = mCamera.mWorld.GetTranslation();

		if (inputHandler.GetKeycodeState(GLFW_KEY_R) & JUST_PRESSED) {
			shader_reload_failed = !program_manager.ReloadAllPrograms();
			if (shader_reload_failed)
				tinyfd_notifyPopup("Shader Program Reload Error",
				                   "An error occurred while reloading shader programs; see the logs for details.\n"
				                   "Rendering is suspended until the issue is solved. Once fixed, just reload the shaders again.",
				                   "error");
		}
		if (inputHandler.GetKeycodeState(GLFW_KEY_F3) & JUST_RELEASED)
			show_logs = !show_logs;
		if (inputHandler.GetKeycodeState(GLFW_KEY_F2) & JUST_RELEASED)
			show_gui = !show_gui;
		if (inputHandler.GetKeycodeState(GLFW_KEY_F11) & JUST_RELEASED)
			mWindowManager.ToggleFullscreenStatusForWindow(window);

		if ((inputHandler.GetKeycodeState(GLFW_KEY_LEFT) & PRESSED) && !(inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			dtl += std::chrono::duration<float>(deltaTimeUs).count();
			dtl = std::min(dtl, 3.0f);
			current_move_acceleration.x = current_move_acceleration.x + accel_lerp * ((current_move_velocity.x - -move_velocity.x*dtl) / std::chrono::duration<float>(deltaTimeUs).count() - current_move_acceleration.x);
			current_move_acceleration.x = glm::clamp(current_move_acceleration.x, -0.8f, 0.8f);
			accel_lerp += 0.001f;
			current_move_velocity.x = -move_velocity.x * dtl;
			demo_sphere.get_transform().Translate(glm::vec3(current_move_velocity.x, 0.0f, 0.0f));
			lkey = true;
			lpress = true;
		}
		if ((inputHandler.GetKeycodeState(GLFW_KEY_RIGHT) & PRESSED) && !(inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			dtr += std::chrono::duration<float>(deltaTimeUs).count();
			dtr = std::min(dtr, 3.0f);
			current_move_acceleration2.x = current_move_acceleration2.x + accel_lerp * ((current_move_velocity2.x - move_velocity.x*dtr) / std::chrono::duration<float>(deltaTimeUs).count() - current_move_acceleration2.x);
			current_move_acceleration2.x = glm::clamp(current_move_acceleration2.x, -0.8f, 0.8f);
			accel_lerp += 0.001f;
			current_move_velocity2.x = move_velocity.x * dtr;
			demo_sphere.get_transform().Translate(glm::vec3(current_move_velocity2.x, 0.0f, 0.0f));
			rkey = true;
			rpress = true;
		}
		if ((inputHandler.GetKeycodeState(GLFW_KEY_UP) & PRESSED) && !(inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			dtu += std::chrono::duration<float>(deltaTimeUs).count();
			dtu = std::min(dtu, 3.0f);
			current_move_acceleration.z = current_move_acceleration.z + accel_lerp * ((current_move_velocity.z - -move_velocity.z*dtu) / std::chrono::duration<float>(deltaTimeUs).count() - current_move_acceleration.z);
			current_move_acceleration.z = glm::clamp(current_move_acceleration.z, -0.8f, 0.8f);
			accel_lerp += 0.001f;
			current_move_velocity.z = -move_velocity.z * dtu;
			demo_sphere.get_transform().Translate(glm::vec3(0.0f, 0.0f, current_move_velocity.z));
			ukey = true;
			upress = true;
		}
		if ((inputHandler.GetKeycodeState(GLFW_KEY_DOWN) & PRESSED) && !(inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			dtd += std::chrono::duration<float>(deltaTimeUs).count();
			dtd = std::min(dtd, 3.0f);
			current_move_acceleration2.z = current_move_acceleration2.z + accel_lerp * ((current_move_velocity2.z - move_velocity.z*dtd) / std::chrono::duration<float>(deltaTimeUs).count() - current_move_acceleration2.z);
			current_move_acceleration2.z = glm::clamp(current_move_acceleration2.z, -0.8f, 0.8f);
			accel_lerp += 0.001f;
			current_move_velocity2.z = move_velocity.z * dtd;
			demo_sphere.get_transform().Translate(glm::vec3(0.0f, 0.0f, current_move_velocity2.z));
			dkey = true;
			dpress = true;
		}
		if (inputHandler.GetKeycodeState(GLFW_KEY_LEFT) & RELEASED && lkey) {
			lpress = false;
			ldeacc = true;
			if (dtl > 0.0f) {
				accel_lerp -= 0.001f;
				dtl -= std::chrono::duration<float>(deltaTimeUs).count() * 2;
				current_move_velocity.x = -move_velocity.x * dtl;
				demo_sphere.get_transform().Translate(glm::vec3(current_move_velocity.x, 0.0f, 0.0f));
			}
			else {
				dtl = 0.0f;
				accel_lerp = 0.0f;
				lkey = false;
			}
		}
		if (inputHandler.GetKeycodeState(GLFW_KEY_RIGHT) & RELEASED && rkey) {
			rpress = false;
			rdeacc = true;
			if (dtr > 0.0f) {
				dtr -= std::chrono::duration<float>(deltaTimeUs).count() * 2;
				accel_lerp -= 0.001f;
				current_move_velocity2.x = move_velocity.x * dtr;
				demo_sphere.get_transform().Translate(glm::vec3(current_move_velocity2.x, 0.0f, 0.0f));
			}
			else {
				dtr = 0.0f;
				accel_lerp = 0.0f;
				rkey = false;
			}
		}
		if (inputHandler.GetKeycodeState(GLFW_KEY_UP) & RELEASED && ukey) {
			upress = false;
			udeacc = true;
			if (dtu > 0.0f) {
				accel_lerp -= 0.001f;
				dtu -= std::chrono::duration<float>(deltaTimeUs).count() * 2;
				current_move_velocity.z = -move_velocity.z * dtu;
				demo_sphere.get_transform().Translate(glm::vec3(0.0f, 0.0f, current_move_velocity.z));
			}
			else {
				dtu = 0.0f;
				accel_lerp = 0.0f;
				ukey = false;
			}
		}
		if (inputHandler.GetKeycodeState(GLFW_KEY_DOWN) & RELEASED && dkey) {
			dpress = false;
			ddeacc = true;
			if (dtd > 0.0f) {
				accel_lerp -= 0.001f;
				dtd -= std::chrono::duration<float>(deltaTimeUs).count() * 2;
				current_move_velocity2.z = move_velocity.z * dtd;
				demo_sphere.get_transform().Translate(glm::vec3(0.0f, 0.0f, current_move_velocity2.z));
			}
			else {
				dtd = 0.0f;
				accel_lerp = 0.0f;
				dkey = false;
			}
		}

		if ((inputHandler.GetKeycodeState(GLFW_KEY_LEFT) & PRESSED) && (inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			current_angular_velocity = glm::vec3(0.0f, 0.0f, angular_velocity.z);
			demo_sphere.get_transform().RotateZ(angular_velocity.z);
		}
		if ((inputHandler.GetKeycodeState(GLFW_KEY_RIGHT) & PRESSED) && (inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			current_angular_velocity = glm::vec3(0.0f, 0.0f, -angular_velocity.z);
			demo_sphere.get_transform().RotateZ(-angular_velocity.z);
		}
		if ((inputHandler.GetKeycodeState(GLFW_KEY_UP) & PRESSED) && (inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			current_angular_velocity = glm::vec3(-angular_velocity.x, 0.0f, 0.0f);
			demo_sphere.get_transform().RotateX(-angular_velocity.x);
		}
		if ((inputHandler.GetKeycodeState(GLFW_KEY_DOWN) & PRESSED) && (inputHandler.GetKeycodeState(GLFW_KEY_RIGHT_SHIFT) & PRESSED)) {
			current_angular_velocity = glm::vec3(angular_velocity.x, 0.0f, 0.0f);
			demo_sphere.get_transform().RotateX(angular_velocity.x);
		}

		glm::vec2 wobble = glm::vec2(0.0f); //Handles wobble left and "up" (away from the camera)
		wobble.x = -current_move_acceleration.x * 50.0f;
		wobble.y = -current_move_acceleration.z * 50.0f;

		glm::vec2 wobble2 = glm::vec2(0.0f); //Handles wobble right and "down" (towards the camera)
		wobble2.x = -current_move_acceleration2.x * 50.0f;
		wobble2.y = -current_move_acceleration2.z * 50.0f;

		if (ldeacc) {
			wobble.x = wobble.x + ((deltax*sin(sinx)*0.5f)+0.5f) * (-wobble.x - wobble.x);
			sinx += 0.1f;
			deltax = std::expf(-varx);
			varx += 0.01f;
			if (deltax < 0.001f || lpress) {
				deltax = 1.0f;
				sinx = 0.0f;
				varx = 0.0f;
				ldeacc = false;
				if (!lpress) {
					current_move_acceleration.x = 0.0f;
				}
			}

		}
		if (rdeacc) {
			wobble2.x = wobble2.x + ((deltax2 * sin(sinx2) * 0.5f) + 0.5f) * (-wobble2.x - wobble2.x);
			sinx2 += 0.1f;
			deltax2 = std::expf(-varx2);
			varx2 += 0.01f;
			if (deltax2 < 0.001f || rpress) {
				deltax2 = 1.0f;
				sinx2 = 0.0f;
				varx2 = 0.0f;
				rdeacc = false;
				if (!rpress) {
					current_move_acceleration2.x = 0.0f;
				}
			}

		}

		if (udeacc) {
			wobble.y = wobble.y + ((deltaz * sin(sinz) * 0.5f) + 0.5f) * (-wobble.y - wobble.y);
			sinz += 0.1f;
			deltaz = std::expf(-varz);
			varz += +0.01f;
			if (deltaz < 0.001f || upress) {
				deltaz = 1.0f;
				sinz = 0.0f;
				varz = 0.0f;
				udeacc = false;
				if (!upress) {
					current_move_acceleration.z = 0.0f;
				}
			}

		}

		if (ddeacc) {
			wobble2.y = wobble2.y + ((deltaz2 * sin(sinz2) * 0.5f) + 0.5f) * (-wobble2.y - wobble2.y);
			sinz2 += 0.1f;
			deltaz2 = std::expf(-varz2);
			varz2 += 0.01f;
			if (deltaz2 < 0.001f || dpress) {
				deltaz2 = 1.0f;
				sinz2 = 0.0f;
				varz2 = 0.0f;
				ddeacc = false;
				if (!dpress) {
					current_move_acceleration2.z = 0.0f;
				}
			}

		}

		sinWobble = wobble + wobble2;

		if (accel_lerp < 0.0f) {
			accel_lerp = 0.0f;
		}
		else if (accel_lerp > 1.0f) {
			accel_lerp = 1.0f;
		}

		// Retrieve the actual framebuffer size: for HiDPI monitors,
		// you might end up with a framebuffer larger than what you
		// actually asked for. For example, if you ask for a 1920x1080
		// framebuffer, you might get a 3840x2160 one instead.
		// Also it might change as the user drags the window between
		// monitors with different DPIs, or if the fullscreen status is
		// being toggled.
		int framebuffer_width, framebuffer_height;
		glfwGetFramebufferSize(window, &framebuffer_width, &framebuffer_height);
		glViewport(0, 0, framebuffer_width, framebuffer_height);


		mWindowManager.NewImGuiFrame();


		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		bonobo::changePolygonMode(polygon_mode);


		skybox.render(mCamera.GetWorldToClipMatrix());
		demo_sphere.render(mCamera.GetWorldToClipMatrix());


		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		bool opened = ImGui::Begin("Scene Control", nullptr, ImGuiWindowFlags_None);
		if (opened) {
			auto const cull_mode_changed = bonobo::uiSelectCullMode("Cull mode", cull_mode);
			if (cull_mode_changed) {
				changeCullMode(cull_mode);
			}
			bonobo::uiSelectPolygonMode("Polygon mode", polygon_mode);
			auto demo_sphere_selection_result = program_manager.SelectProgram("Demo sphere", demo_sphere_program_index);
			if (demo_sphere_selection_result.was_selection_changed || true) {
				demo_sphere.set_program(demo_sphere_selection_result.program, phong_set_uniforms);
			}
			ImGui::Separator();
			ImGui::SliderFloat("Fill Amount", &fillAmount, -1.5f, 1.5f);
		}
		ImGui::End();

		opened = ImGui::Begin("Render Time", nullptr, ImGuiWindowFlags_None);
		if (opened)
			ImGui::Text("%.3f ms", std::chrono::duration<float, std::milli>(deltaTimeUs).count());
		ImGui::End();

		if (show_logs)
			Log::View::Render();
		mWindowManager.RenderImGuiFrame(show_gui);

		glfwSwapBuffers(window);
	}
}

int main()
{
	std::setlocale(LC_ALL, "");

	Bonobo framework;

	try {
		edaf80::Assignment3 assignment3(framework.GetWindowManager());
		assignment3.run();
	} catch (std::runtime_error const& e) {
		LogError(e.what());
	}
}

# EDAN35-Project
Project in the course High Performance Computer Graphics.\
This program contains a shader that can make a 3D object appear to have liquids inside without the need for fluid simulations.\
Written in C++ and OpenGL.\
For a demonstration please view:
[liquid_in_a_container.mp4](liquid_in_a_container.mp4)\
The report can be viewed here:
[EDAN35_Project.pdf](EDAN35_Project.pdf)
